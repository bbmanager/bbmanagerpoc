§package ru.bbmanager.poc.dao.impl;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.bbmanager.poc.dao.UserDAO;
import ru.bbmanager.poc.model.UserModel;

@Repository("userDAO")
public class UserDAOImpl implements UserDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public UserModel findById(Integer id) {
        Preconditions.checkNotNull(id, "Parameter 'id' can't be null!");

        return (UserModel) sessionFactory.getCurrentSession().get(UserModel.class, id);
    }

    @Override
    public UserModel findByEmail(String email) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(email), "Parameter 'email' can't be null or empty!");

        Query query = sessionFactory.getCurrentSession().createQuery("FROM UserModel WHERE email = :email");
        query.setString("email", email);
        return (UserModel) query.uniqueResult();
    }

    @Override
    public Integer save(UserModel user) {
        Preconditions.checkNotNull(user, "Parameter 'user' can't be null!");

        return (Integer) sessionFactory.getCurrentSession().save(user);
    }
}
