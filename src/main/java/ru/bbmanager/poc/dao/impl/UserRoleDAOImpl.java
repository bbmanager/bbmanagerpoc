package ru.bbmanager.poc.dao.impl;

import com.google.common.base.Preconditions;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.bbmanager.poc.dao.UserRoleDAO;
import ru.bbmanager.poc.model.security.UserRoleModel;

@Repository("userRoleDAO")
public class UserRoleDAOImpl implements UserRoleDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public UserRoleModel save(UserRoleModel userRole) {
        Preconditions.checkNotNull(userRole, "Parameter 'userRole' can't be null!");

        sessionFactory.getCurrentSession().save(userRole);
        return userRole;
    }
}
