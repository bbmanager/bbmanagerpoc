package ru.bbmanager.poc.dao;

import ru.bbmanager.poc.model.UserModel;

public interface UserDAO {
    UserModel findById(Integer id);
    UserModel findByEmail(String email);

    Integer save(UserModel object);
}
