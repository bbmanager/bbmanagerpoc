package ru.bbmanager.poc.dao;

import ru.bbmanager.poc.model.security.UserRoleModel;

public interface UserRoleDAO {
    UserRoleModel save(UserRoleModel userRole);
}
