package ru.bbmanager.poc.service.impl;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.bbmanager.poc.dao.UserDAO;
import ru.bbmanager.poc.model.UserModel;
import ru.bbmanager.poc.service.UserService;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public UserModel getCurrentUser() {
        final ServletRequestAttributes attr =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        final HttpSession session = attr.getRequest().getSession();

        return (UserModel) session.getAttribute("user");
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public UserModel getUserById(Integer id) {
        Preconditions.checkNotNull(id, "Parameter 'id' can't be null!");

        return userDAO.findById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public UserModel getUserByEMail(String email) {
        Preconditions.checkNotNull(email, "Parameter 'email' can't be null!");

        return userDAO.findByEmail(email);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public Integer createUser(UserModel user) {
        Preconditions.checkNotNull(user, "Parameter 'user' can't be null!");

        user.setRegistrationDate(new Date());
        return userDAO.save(user);
    }

    @Override
    public UserModel save(UserModel user) {
        Preconditions.checkNotNull(user, "Parameter 'user' can't be null!");

        userDAO.save(user);

        return user;
    }
}
