package ru.bbmanager.poc.service.impl;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.bbmanager.poc.dao.UserRoleDAO;
import ru.bbmanager.poc.model.UserModel;
import ru.bbmanager.poc.model.security.UserRoleModel;
import ru.bbmanager.poc.service.UserRoleService;

import java.util.HashSet;

@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleDAO userRoleDAO;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public UserRoleModel grantRole(UserModel user, String role) {
        final UserRoleModel userRole = new UserRoleModel(user, role);

        final HashSet<UserRoleModel> userRoles = new HashSet<>(user.getRoles());
        userRoles.add(userRole);
        user.setRoles(userRoles);

        return save(userRole);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public UserRoleModel save(UserRoleModel userRole) {
        Preconditions.checkNotNull(userRole, "Parameter 'userRole' can't be null!");

        return userRoleDAO.save(userRole);
    }


}
