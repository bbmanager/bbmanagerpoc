package ru.bbmanager.poc.service;

import ru.bbmanager.poc.model.UserModel;

public interface UserService {
    UserModel getCurrentUser();

    UserModel getUserById(Integer id);
    UserModel getUserByEMail(String email);

    Integer createUser(UserModel user);

    UserModel save(UserModel user);
}
