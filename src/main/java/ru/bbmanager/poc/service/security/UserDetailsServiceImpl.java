package ru.bbmanager.poc.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bbmanager.poc.dao.UserDAO;
import ru.bbmanager.poc.model.UserModel;
import ru.bbmanager.poc.model.security.UserRoleModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserDAO userDAO;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        UserModel user = userDAO.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("No user found with email: "+ email);
        }

        return buildUserForAuthentication(user);

    }

    private User buildUserForAuthentication(UserModel user) {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        return new User(user.getEmail(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, buildUserAuthority(user.getRoles()));

    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRoleModel> userRoles) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        for (UserRoleModel userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
        }

        return new ArrayList<GrantedAuthority>(setAuths);
    }
}
