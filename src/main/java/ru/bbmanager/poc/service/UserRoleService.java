package ru.bbmanager.poc.service;

import ru.bbmanager.poc.model.UserModel;
import ru.bbmanager.poc.model.security.UserRoleModel;

public interface UserRoleService {
    UserRoleModel grantRole(UserModel user, String role);

    UserRoleModel save(UserRoleModel userRole);
}
