package ru.bbmanager.poc.model.security;

import ru.bbmanager.poc.model.UserModel;

import javax.persistence.*;

@Entity
@Table(name = "userRoles")
public class UserRoleModel {
    public static final String ROLE_USER = "ROLE_USER";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    private UserModel user;

    @Column(name = "role", nullable = false, length = 45)
    private String role;

    public UserRoleModel() {

    }

    public UserRoleModel(UserModel user, String role) {
        this.user = user;
        this.role = role;
    }

    public Integer getId() {
        return this.id;
    }

    public UserModel getUserModel() {
        return this.user;
    }

    public void setUserModel(UserModel user) {
        this.user = user;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
